package com.JavanIntern.CountVowel.controller;

import com.JavanIntern.CountVowel.dto.CountVowelDto;
import com.JavanIntern.CountVowel.service.CountVowelService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("")
public class CountVowelController {

    private CountVowelService countVowelService;

    @RequestMapping("")
    public String countVowel(CountVowelDto countVowelDto, Model model) {
        String title = "Count Vowel Web App";

        String result = "";

        String sentence = countVowelDto.getSentence();
        String calculate = countVowelDto.getCalculate();

        if (calculate != null) {
            result += countVowelService.calculateSentence(sentence);
            model.addAttribute("sentence", sentence);
            model.addAttribute("result", result);
        }

        model.addAttribute("title", title);
        return "index";
    }
}
