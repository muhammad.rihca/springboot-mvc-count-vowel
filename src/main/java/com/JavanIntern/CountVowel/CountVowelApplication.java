package com.JavanIntern.CountVowel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CountVowelApplication {

	public static void main(String[] args) {
		SpringApplication.run(CountVowelApplication.class, args);
	}

}
