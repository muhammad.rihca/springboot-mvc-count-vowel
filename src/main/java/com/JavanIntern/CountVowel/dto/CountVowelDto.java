package com.JavanIntern.CountVowel.dto;

public class CountVowelDto {
    private String sentence;
    private String calculate;

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public String getCalculate() {
        return calculate;
    }

    public void setCalculate(String calculate) {
        this.calculate = calculate;
    }
}
