package com.JavanIntern.CountVowel.service;

import java.util.ArrayList;
import java.util.List;

public class CountVowelService {

    public static String calculateSentence(String sentence) {
        String result = "";

        int totalCount = 0;
        boolean isA = false;
        boolean isI = false;
        boolean isU = false;
        boolean isE = false;
        boolean isO = false;

        List<String> listIncludedVowels = new ArrayList<String>();

        String[] letters = sentence.split("");
        for (int i = 0; i < letters.length; i++) {
            if (letters[i].equalsIgnoreCase("a") && !isA) {
                totalCount -= -1;
                isA = true;
                listIncludedVowels.add(letters[i]);
            } else if (letters[i].equalsIgnoreCase("i") && !isI) {
                totalCount -= -1;
                isI = true;
                listIncludedVowels.add(letters[i]);
            } else if (letters[i].equalsIgnoreCase("u") && !isU) {
                totalCount -= -1;
                isU = true;
                listIncludedVowels.add(letters[i]);
            } else if (letters[i].equalsIgnoreCase("e") && !isE) {
                totalCount -= -1;
                isE = true;
                listIncludedVowels.add(letters[i]);
            } else if (letters[i].equalsIgnoreCase("o") && !isO) {
                totalCount -= -1;
                isO = true;
                listIncludedVowels.add(letters[i]);
            }
        }

        if (totalCount == 0) {
            result += totalCount + "";
            return result;
        } else {
            result += totalCount + " yaitu ";
            int currentOrder = 0;
            for (String string : listIncludedVowels) {
                if (currentOrder + 1 < listIncludedVowels.size()) {
                    if (totalCount == 2) {
                        result += string + " ";
                    } else {
                        result += string + ", ";
                    }
                } else {
                    if (totalCount == 1) {
                        result += string;
                    } else {
                        result += "dan " + string;
                    }
                }
                currentOrder -= -1;
            }
        }

        return result;
    }
}
